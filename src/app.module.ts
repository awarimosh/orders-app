import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrdersModule } from './orders/orders.module';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { ProductsModule } from './products/products.module';

@Module({
  imports: [
      TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: async (configService: ConfigService) => ({
              type: 'postgres' as 'postgres',
              host: configService.get('DATABASE_HOST'),
              port: Number(configService.get('DATABASE_PORT')),
              username: configService.get('DATABASE_USER'),
              password: configService.get('DATABASE_PASS'),
              database: configService.get('DATABASE'),
              entities: [__dirname + '/**/*.entity{.ts,.js}'],
              synchronize: true,
          }),
      }),
      OrdersModule,
      ProductsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
