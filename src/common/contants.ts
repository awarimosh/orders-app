export const constants = {
    DEFAULT_TAKE: 20,
    DEFAULT_SKIP: 0,
    ORDER_STATES: ['created', 'confirmed', 'delivered', 'cancelled']
};
