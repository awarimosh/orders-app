import {
    IsNotEmpty, IsUUID, ValidateNested, IsOptional, IsBoolean, IsIn, ValidateIf, Min
} from 'class-validator';
import {Type} from 'class-transformer';
import {constants} from "../../common/contants";

export class Body {
    @IsOptional()
    @IsIn(constants.ORDER_STATES)
    state: string;

    @IsOptional()
    @Min(0)
    amount: number;

    @IsOptional()
    @IsBoolean()
    is_deleted: boolean;

}

export class OrderUpdateDto {
    @IsNotEmpty()
    @Type(() => Body)
    @ValidateNested({each: true})
    body: Body;
}