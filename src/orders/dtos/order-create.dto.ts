import {
    IsNotEmpty, ValidateNested, IsArray, IsInt, Min
} from 'class-validator';
import { Type } from 'class-transformer';

export class Body {
    @IsArray()
    products: string;

    @IsInt()
    @Min(0)
    amount: number;
}

export class OrderCreateDto {
    @IsNotEmpty()
    @Type(() => Body)
    @ValidateNested({ each: true })
    body: Body;
}