import {
    Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, Generated, OneToMany,
    OneToOne, JoinColumn, JoinTable, ManyToMany
} from 'typeorm';
import {constants} from "../../common/contants";
import {Product} from "../../products/entities/product.entity";

@Entity()
export class Order {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ enum: constants.ORDER_STATES, default: 'created' })
    state: string;

    @Column('decimal', { precision: 7, scale: 2, default: 0 })
    amount: number;

    @Column({ default: false })
    is_deleted: boolean;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column({ nullable: true })
    date: Date;

    @ManyToMany(type => Product, product => product.orders)
    @JoinTable()
    products: Product[];
}




