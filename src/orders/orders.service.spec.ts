import {Repository} from "typeorm";
import {Order} from "./entities/order.entity";
import {Test, TestingModule} from "@nestjs/testing";
import {getRepositoryToken} from "@nestjs/typeorm";
import {OrdersService} from "./orders.service";
import {ClientsModule, RpcException, Transport} from '@nestjs/microservices';

export class OrdersRepositoryFake {
    public create(): void {
    }

    public async save(): Promise<void> {
    }

    public async remove(): Promise<void> {
    }

    public async findOne(): Promise<void> {
    }
}

describe('OrdersService', () => {
    let ordersService: OrdersService;
    let ordersRepository: Repository<Order>;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ClientsModule.register([{name: 'PAYMENT_SERVICE', transport: Transport.TCP, options: {port: 3002}}]),
            ],
            providers: [
                OrdersService,
                {
                    provide: getRepositoryToken(Order),
                    useClass: OrdersRepositoryFake,
                },
            ]
        }).compile();

        ordersService = module.get(OrdersService);
        ordersRepository = module.get(getRepositoryToken(Order));
    });

    describe('creating a order', () => {
        it('throws an error when no products', async () => {
            const data = {body: {products: []}};
            expect.assertions(2);

            try {
                await ordersService.create(data);
            } catch (e) {
                expect(e).toBeInstanceOf(RpcException);
                expect(e.message).toBe('Products missing');
            }
        });
    });
});