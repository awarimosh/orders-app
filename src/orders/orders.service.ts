import { Inject, Injectable } from '@nestjs/common';
import { Order } from './entities/order.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ClientProxy, RpcException } from '@nestjs/microservices';
const moment = require('moment-timezone');

@Injectable()
export class OrdersService {
    constructor(
        @InjectRepository(Order)
        private readonly orderRepository: Repository<Order>,
        @Inject('PAYMENT_SERVICE') private clientPayment: ClientProxy,) {
            
    }

    async create(data: any): Promise<Order> {
        try {
            let model = new Order();
            model.amount = data.body.amount;
            if(data.body.date)
                model.date = data.body.date;
            else
                model.date = new Date();
            if(!data.body.products || data.body.products.length <= 0){
                throw new RpcException("Products missing");
            }
            model.products = data.body.products;
            await this.orderRepository.save(model);
            const paymentData = {
                order : model.id,
                amount : model.amount,
                tax : 0,
                date : new Date(),
                returnURL : '',
                shipname : `order-${data.body.products[0].name}-${new Date().toISOString()}`,
            };
            return this.clientPayment.send<any>({ cmd: 'payments-create' }, {
                body: paymentData
            }).toPromise().then(async (response) => {
                console.log('payments-create response', response.status);
                if(response.status === 'COMPLETED') {
                    model.state = 'confirmed';
                    this.fakeDelivery(model);
                }
                else{
                    model.state = 'cancelled';
                }
                await this.orderRepository.save(model);
                return model;
            }).catch((error) => {
                console.log(error);
                return model;
            });
        } catch (error) {
            throw new RpcException(error);
        }
    }

    async fakeDelivery(model:Order): Promise<Order>{
        await new Promise(resolve => setTimeout(resolve, 20000));
        model.state = 'delivered';
        await this.orderRepository.save(model);
        return model;
    }

    async find(data): Promise<{ result: Order[], total: number, page: number, count: number }> {
        let query = { where: { }, };
        if (data.query) {
            if (data.query.take && data.query.skip) {
                query = { ...query, ...{ take: data.query.take, skip: data.query.skip } }
            }

            if (data.query.state) {
                query.where = { ...query.where, ...{ state: data.query.state } }
            }

            if (data.query.is_deleted !== null && data.query.is_deleted !== undefined) {
                query.where = {...query.where, ...{is_deleted: data.query.is_deleted}}
            }
            else {
                query.where = {...query.where, ...{is_deleted: false}}
            }

            if (data.query.amount) {
                query.where = {...query.where, ...{amount: data.query.amount}}
            }

        }
        const [result, total] = await this.orderRepository.findAndCount({ ...query, order: { createdAt: 'DESC' }, relations: ['products'] });
        return {
            result: result,
            total: total,
            page: data.query && data.query.take && data.query.skip ? ((data.query.take * 1 + data.query.skip * 1) / data.query.take) : 0,
            count: data.query ? data.query.take ? parseInt(data.query.take) : result.length : null
        }
    }

    async findOne(data): Promise<any> {
        return await this.orderRepository.findOne({
            where: { ...data.params, is_deleted: false }
        })
    }

    async update(data): Promise<any> {
        let model = await this.orderRepository.findOne({
            where: { ...data.params, is_deleted: false }
        });

        if (model) {
            if(data.body.state)
                model.state = data.body.state;

            if(data.body.amount)
                model.amount = data.body.amount;

            if(data.body.products)
                model.products = data.body.products;
            await this.orderRepository.save(model);
            return model;
        } else {
            throw new RpcException("Order not found");
        }
    }

    async delete(data): Promise<any> {
        let model = await this.orderRepository.findOne({
            where: { ...data.params, is_deleted: false }
        });

        if (model) {
            model.is_deleted = true;
            await this.orderRepository.save(model);
            return model
        }  else {
            throw new RpcException("Order not found");
        }
    }
}