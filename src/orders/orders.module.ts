import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { ConfigModule } from 'src/config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/order.entity'
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
      ConfigModule,
      TypeOrmModule.forFeature([Order]),
      ClientsModule.register([{ name: 'PAYMENT_SERVICE', transport: Transport.TCP, options: { port: 3002 } }]),
],
  providers: [OrdersService],
  controllers: [OrdersController]
})
export class OrdersModule { }