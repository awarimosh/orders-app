import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { OrdersService } from './orders.service';
import { OrderCreateDto } from "./dtos/order-create.dto";
import { OrderFindDto } from "./dtos/order-find.dto";
import { OrderUpdateDto } from "./dtos/order-update.dto";

@Controller('orders')
export class OrdersController {
    constructor(private readonly ordersService: OrdersService) {

    }

    @MessagePattern({ cmd: 'orders-create' })
    async create(data: OrderCreateDto): Promise<any> {
        return await this.ordersService.create(data);
    }

    @MessagePattern({ cmd: 'orders-find' })
    async find(data?: OrderFindDto): Promise<any> {
        return await this.ordersService.find(data);
    }

    @MessagePattern({ cmd: 'orders-find-one' })
    async findOne(data: any): Promise<any> {
        return await this.ordersService.findOne(data);
    }

    @MessagePattern({ cmd: 'orders-update' })
    async update(data: OrderUpdateDto): Promise<any> {
        return await this.ordersService.update(data);
    }

    @MessagePattern({ cmd: 'orders-delete' })
    async delete(data: any): Promise<any> {
        return await this.ordersService.delete(data);
    }
}
