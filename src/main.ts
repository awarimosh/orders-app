import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { ValidationPipe } from './common/validation.pipe';

async function bootstrap() {
    const app = await NestFactory.createMicroservice(AppModule, {
        transport: Transport.TCP,
        options: {
            port: 3001
        }
    });
    app.useGlobalPipes(new ValidationPipe());
    app.listen(() => console.log('Order service is listening on port 3001'));
}
bootstrap();

