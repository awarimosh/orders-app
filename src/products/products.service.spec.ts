import {Repository} from "typeorm";
import {Product} from "./entities/product.entity";
import {Test, TestingModule} from "@nestjs/testing";
import {getRepositoryToken} from "@nestjs/typeorm";
import {ProductsService} from "./products.service";
import {ClientsModule, RpcException, Transport} from '@nestjs/microservices';

export class ProductsRepositoryFake {
    public create(): void {
    }

    public async update(): Promise<void> {
    }

    public async remove(): Promise<void> {
    }

    public async find(): Promise<void> {
    }

    public async findOne(): Promise<void> {
    }
}

describe('ProductsService', () => {
    let productsService: ProductsService;
    let productsRepository: Repository<Product>;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
            ],
            providers: [
                ProductsService,
                {
                    provide: getRepositoryToken(Product),
                    useClass: ProductsRepositoryFake,
                },
            ]
        }).compile();

        productsService = module.get(ProductsService);
        productsRepository = module.get(getRepositoryToken(Product));
    });

    describe('creating a product', () => {
        it('throws an error when no products', async () => {
            const data = {body: {id: '',name : ''}};
            expect.assertions(2);

            try {
                await productsService.create(data);
            } catch (e) {
                expect(e).toBeInstanceOf(RpcException);
                expect(e.message).toBe('product name missing');
            }
        });
    });
});