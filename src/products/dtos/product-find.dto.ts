import {
    IsNotEmpty, IsUUID, ValidateNested, IsOptional, IsBoolean, IsIn, IsArray, IsString
} from 'class-validator';
import { Type, Transform } from 'class-transformer';

export class ProductFindDto {
    @IsNotEmpty()
    @ValidateNested({ each: true })
    query: string;
}
