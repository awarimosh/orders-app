import {
    IsNotEmpty, ValidateNested, IsOptional, Min, IsInt
} from 'class-validator';
import { Type } from 'class-transformer';

export class Body {
    @IsNotEmpty()
    name: string;

    @IsOptional()
    description: string;

    @IsInt()
    @Min(0)
    price: number;
}

export class ProductCreateDto {
    @IsNotEmpty()
    @Type(() => Body)
    @ValidateNested({ each: true })
    body: Body;
}