import {Injectable} from '@nestjs/common';
import {Product} from "./entities/product.entity";
import {Raw, Repository} from "typeorm";
import {InjectRepository} from "@nestjs/typeorm";
import {RpcException} from "@nestjs/microservices";

@Injectable()
export class ProductsService {
    constructor(@InjectRepository(Product)
                private readonly productRepository: Repository<Product>) {

    }

    async create(data: any): Promise<Product> {
        try {
            let model = new Product();
            if(!data.body.name || data.body.name.length <= 0)
                throw new RpcException('product name missing');
            model.name = data.body.name;
            model.description = data.body.description;
            model.price = data.body.price;
            await this.productRepository.save(model);
            return model;
        } catch (error) {
            throw new RpcException(error);
        }
    }

    async find(data): Promise<{ result: Product[], total: number, page: number, count: number }> {
        let query = {where: {}};
        if (data.query) {
            if (data.query.take && data.query.skip) {
                query = {...query, ...{take: data.query.take, skip: data.query.skip}}
            }

            if (data.query.is_deleted !== null && data.query.is_deleted !== undefined) {
                query.where = {...query.where, ...{is_deleted: data.query.is_deleted}}
            }
            else {
                query.where = {...query.where, ...{is_deleted: false}}
            }

            if (data.query.name) {
                query.where = {...query.where, ...{name: Raw(alias => `${alias} ILIKE '%${data.query.name}%'`)}}
            }

        }
        const [result, total] = await this.productRepository.findAndCount({...query, order: {createdAt: 'DESC'}});
        return {
            result: result,
            total: total,
            page: data.query && data.query.take && data.query.skip ? ((data.query.take * 1 + data.query.skip * 1) / data.query.take) : 0,
            count: data.query ? data.query.take ? parseInt(data.query.take) : result.length : null
        }
    }

    async findOne(data): Promise<any> {
        return await this.productRepository.findOne({
            where: {...data.params, is_deleted: false}
        })
    }

    async update(data): Promise<any> {
        let model = await this.productRepository.findOne({
            where: {...data.params, is_deleted: false}
        });

        if (model) {
            model.name = data.body.name;
            model.description = data.body.description;
            model.price = data.body.price;
            if (data.body.is_deleted !== null && data.body.is_deleted !== undefined)
                model.is_deleted = data.body.is_deleted;

            await this.productRepository.save(model);
            return model;
        } else {
            throw new RpcException("Order not found");
        }
    }

    async delete(data): Promise<any> {
        let model = await this.productRepository.findOne({
            where: {...data.params, is_deleted: false}
        });

        if (model) {
            model.is_deleted = true;
            await this.productRepository.save(model);
            return model
        } else {
            throw new RpcException("Order not found");
        }
    }
}
