import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { ConfigModule } from "../config/config.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Product } from "./entities/product.entity";

@Module({
    imports: [
        ConfigModule,
        TypeOrmModule.forFeature([Product]),
    ],
  providers: [ProductsService],
  controllers: [ProductsController]
})
export class ProductsModule {}
