import { Controller } from '@nestjs/common';
import { MessagePattern } from "@nestjs/microservices";
import { ProductCreateDto } from "./dtos/product-create.dto";
import { ProductsService } from "./products.service";
import { ProductFindDto } from "./dtos/product-find.dto";
import { ProductUpdateDto } from "./dtos/product-update.dto";

@Controller('products')
export class ProductsController {
    constructor(private readonly productsService: ProductsService) {

    }

    @MessagePattern({ cmd: 'products-create' })
    async create(data: ProductCreateDto): Promise<any> {
        return await this.productsService.create(data);
    }

    @MessagePattern({ cmd: 'products-find' })
    async find(data?: ProductFindDto): Promise<any> {
        return await this.productsService.find(data);
    }

    @MessagePattern({ cmd: 'products-find-one' })
    async findOne(data: any): Promise<any> {
        return await this.productsService.findOne(data);
    }

    @MessagePattern({ cmd: 'products-update' })
    async update(data: ProductUpdateDto): Promise<any> {
        return await this.productsService.update(data);
    }

    @MessagePattern({ cmd: 'products-delete' })
    async delete(data: any): Promise<any> {
        return await this.productsService.delete(data);
    }
}
